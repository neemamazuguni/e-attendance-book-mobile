# Electronic Teachers Classroom Attendance Book

## Features

- Academic can login

- Academic can view timetable

- Academic can view timetable for a specific class

- Academic can view timetable for a specific day

- Academic can view timetable for a specific class and day

- Academic can verify the attendance of a teacher
