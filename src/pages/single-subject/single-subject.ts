import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams } from 'ionic-angular';

import { FormBuilder, FormGroup } from '@angular/forms';

import { TimetableService } from '../../providers/timetables';

import { LoadingController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

@Component({
  selector: 'page-single-subject',
  templateUrl: 'single-subject.html'
})
export class SingleSubjectPage {
  id: any;
  subject: any;
  private form: FormGroup;
  loader: any;
  is_student: boolean = false;
  is_teacher: boolean = false;
  is_academic: boolean = false;

  constructor(public storage: Storage, public toastCtrl: ToastController, public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, private timetableService: TimetableService, public loadingCtrl: LoadingController) {
    this.id = this.navParams.get('id');
    this.subject = this.navParams.get('subject');
    console.log(this.id);
    console.log(this.subject);

    this.storage.get("username").then((data) => {
      console.log(data);
      if (data.role == 'student') {
        this.is_student = true;
      }
      if (data.role == 'teacher') {
        this.is_teacher = true;
      }
      if (data.role == 'academic') {
        this.is_academic = true;
      }
    });


    this.form = this.formBuilder.group({
      attended: [],
      verified: [],
      comment: ['']
    });
  }

  submit(id: number) {

    this.presentLoading();

    var attended = 0;
    var verified = 0;
    var comment = "";
    if (this.form.controls['attended'].value != null) {
      attended = 1;
    }
    if (this.form.controls['verified'].value != null) {
      verified = 1;
    }
    comment = this.form.controls['comment'].value;
    console.log(comment);

    this.timetableService.update(this.id, attended, verified, comment).subscribe(res => {
      console.log(res);
      this.presentToast();
      this.loader.dismiss();
      this.navCtrl.pop();
    })
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Sending..."
    });
    this.loader.present();
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Verified successfully',
      duration: 3000
    });
    toast.present();
  }

}
