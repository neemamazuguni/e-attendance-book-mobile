import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';

import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { SingleDayPage } from '../single-day/single-day';

@Component({
  selector: 'page-class-and-day-select',
  templateUrl: 'class-and-day-select.html'
})
export class ClassAndDaySelectPage {
  private form: FormGroup;
  darasa: any;
  siku: any;

  constructor(public navCtrl: NavController, public modalCtrl: ModalController, private formBuilder: FormBuilder) {
 this.form = this.formBuilder.group({
      darasa: ['', Validators.required],
      siku: ['', Validators.required]
    });
  }

  filter(){
    this.darasa = this.form.controls['darasa'].value;
    this.siku = this.form.controls['siku'].value;
    this.navCtrl.push(SingleDayPage, { darasa: this.darasa, siku: this.siku });
  }

}
