import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Nav, NavController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

import { Auth } from '../../providers/auth';

import { HomePage } from '../../pages/home/home';

import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { LoadingController } from 'ionic-angular';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  private user: FormGroup;
  loader: any;

  constructor(public nav: Nav, public navCtrl: NavController, private formBuilder: FormBuilder, public storage: Storage, public auth: Auth, public loadingCtrl: LoadingController, public toastCtrl: ToastController) {
    this.user = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  startlogin() {

    // this.presentLoading();
    // this.auth.startLogin(this.user.controls['username'].value, this.user.controls['password'].value).then((isLoggedIn) => {
    //   if (isLoggedIn) {
    //     this.loader.dismiss();
    //     this.navCtrl.push(HomePage);
    //   } else {
    //     this.presentToast();
    //   }
    // });
    if (this.user.controls['username'].value == 'student') {
      this.storage.set("username", { username: 'student', role: 'student' });
    }
    else if (this.user.controls['username'].value == 'teacher') {
      this.storage.set("username", { username: 'teacher', role: 'teacher' });
    }
    else if (this.user.controls['username'].value == 'academic') {
      this.storage.set("username", { username: 'academic', role: 'academic' });
    } else if (this.user.controls['username'].value == 'mazuguni') {
      this.storage.set("username", { username: 'mazuguni', role: 'student' });
    }
    else if (this.user.controls['username'].value == 'neema') {
      this.storage.set("username", { username: 'neema', role: 'student' });
    }
    else if (this.user.controls['username'].value == 'josephine') {
      this.storage.set("username", { username: 'josephine', role: 'teacher' });
    } else {
      this.storage.set("username", { username: 'user', role: 'user' });
    }

    this.nav.setRoot(HomePage);

  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Authenticating..."
    });
    this.loader.present();
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: '',
      duration: 3000
    });
    toast.present();
  }

}
