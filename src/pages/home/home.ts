import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Nav, NavController } from 'ionic-angular';

import { WeeklySummaryPage } from '../../pages/weekly-summary/weekly-summary';

import { SingleDayPage } from '../../pages/single-day/single-day';
import { LoginPage } from '../../pages/login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  rootPage: any;
  username: any;
  is_student: boolean = false;
  is_teacher: boolean = false;
  is_academic: boolean = false;

  constructor(public nav: Nav, public navCtrl: NavController, public storage: Storage) {
    this.storage.get("username").then((data) => {
      console.log(data);
      this.username = data.role;
      if (data == null) {
        this.rootPage = LoginPage;
      } else {
        if (data.role == 'student') {
          this.is_student = true;
        }
        if (data.role == 'teacher') {
          this.is_teacher = true;
        }
        if (data.role == 'academic') {
          this.is_academic = true;
        }
      }
    });
  }

  goToSingleDay() {
    this.navCtrl.push(SingleDayPage);
  }

  logout() {
    console.log('loggin out');
    this.storage.set("username", null);
    this.nav.setRoot(LoginPage);
  }

  goToWeeklySummary() {
    this.navCtrl.push(WeeklySummaryPage);
  }

}
