import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { ClassAndDaySelectPage } from '../../pages/class-and-day-select/class-and-day-select';
import { SingleSubjectPage } from '../../pages/single-subject/single-subject';

import { Timetable } from '../../models/timetables';

import { TimetableService } from '../../providers/timetables';

@Component({
  selector: 'page-single-day',
  templateUrl: 'single-day.html'
})
export class SingleDayPage {
  darasa: any = "Form I";
  siku: any = "Monday";
  timetables: Timetable[];

  constructor(public navCtrl: NavController, public navParams: NavParams, private timetableService: TimetableService) {

    if (this.navParams.get('darasa') != null) {
      this.darasa = this.navParams.get('darasa');
    }
    if (this.navParams.get('siku') != null) {
      this.siku = this.navParams.get('siku');
    }

    timetableService.load(this.darasa, this.siku).subscribe(timetables => {
      this.timetables = timetables;
      // console.log(this.timetables);
    });

  }

  presentOptions() {
    this.navCtrl.push(ClassAndDaySelectPage);
  }

  goToSingleSubject(id: number, subject: string) {
    this.navCtrl.push(SingleSubjectPage, { id: id, subject: subject });
  }

}
