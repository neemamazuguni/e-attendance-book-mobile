import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { SingleDayPage } from '../../pages/single-day/single-day';

import { Attendance } from '../../models/attendances';

import { TimetableService } from '../../providers/timetables';

@Component({
  selector: 'page-weekly-summary',
  templateUrl: 'weekly-summary.html'
})
export class WeeklySummaryPage {
  attendances: Attendance[];

  constructor(public navCtrl: NavController, private timetableService: TimetableService) {
    timetableService.getSummary().subscribe(attendances => {
      this.attendances = attendances;
      console.log(this.attendances);
    });
  }

  goToSingleDay() {
    this.navCtrl.push(SingleDayPage);
  }


}
