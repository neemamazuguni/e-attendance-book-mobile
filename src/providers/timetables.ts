import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { Timetable } from '../models/timetables';
import { Attendance } from '../models/attendances';

@Injectable()
export class TimetableService {
  appApiUrl = 'http://localhost:88/api';

  constructor(public http: Http) {
    console.log('Hello Timetable Provider');
  }

  load(darasa: string, siku: string): Observable<Timetable[]> {
    return this.http.get(this.appApiUrl + '/timetables.php?darasa=' + darasa + '&siku=' + siku)
      .map(res => <Timetable[]>res.json());
  }

  update(id: number, is_attended: number, is_verified: number, comment: string) {
    return this.http.get(this.appApiUrl + '/update-timetable.php?id=' + id + '&attended=' + is_attended + '&verified=' + is_verified + '&comment=' + comment)
      ;
  }

  getSummary() {
    return this.http.get(this.appApiUrl + '/summary.php')
      .map(res => <Attendance[]>res.json());
  }

}
