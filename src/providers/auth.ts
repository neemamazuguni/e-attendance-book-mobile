import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class Auth {
  appApiUrl = 'http://localhost:88/api';
  validity: any = false;

  constructor(public storage: Storage, public http: Http) {
    console.log('Hello Auth Provider');
  }

  login() {

    return new Promise((resolve) => {
      resolve(this.authenticate());
    });

  }

  startLogin(username: string, password: string) {
    console.log('here');

    this.performLogin(username, password).subscribe(res => {
      console.log('res = ' + res);
      if (res == true) {
        this.validity = true;
        this.storage.set('username', username);
      } else {
        this.validity = false;
      }

      console.log(this.validity);
    })

    return new Promise((resolve) => {
      resolve(this.validity);
    });
  }

  performLogin(username: string, password: string) {
    return this.http.get(this.appApiUrl + '/auth.php?username=' + username + '&password=' + password)
      .map(res => res.json());
  }

  authenticate() {
    return this.storage.get('username').then((data) => {
      if (data == null) {
        return false;
      } else {
        return true;
      }
    })
  }

}
