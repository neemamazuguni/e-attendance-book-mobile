export interface Timetable {
  id: number;
  staff: string;
  subject: string;
}
