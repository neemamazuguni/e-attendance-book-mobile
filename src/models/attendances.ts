export interface Attendance {
  day: string;
  attendance_count: number;
}
