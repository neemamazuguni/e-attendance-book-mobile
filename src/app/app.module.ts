import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';

import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { SingleDayPage } from '../pages/single-day/single-day';
import { WeeklySummaryPage } from '../pages/weekly-summary/weekly-summary';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { SingleSubjectPage } from '../pages/single-subject/single-subject';

import { ClassAndDaySelectPage } from '../pages/class-and-day-select/class-and-day-select';

import { ProfilePage } from '../pages/profile/profile';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Auth } from '../providers/auth';
import { TimetableService } from '../providers/timetables';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    SingleDayPage,
    ClassAndDaySelectPage,
    WeeklySummaryPage,
    AboutPage,
    ContactPage,
    SingleSubjectPage,
    ProfilePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    SingleDayPage,
    ClassAndDaySelectPage,
    WeeklySummaryPage,
    AboutPage,
    ContactPage,
    SingleSubjectPage,
    ProfilePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Auth,
    TimetableService
  ]
})
export class AppModule { }
